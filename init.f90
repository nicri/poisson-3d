! **************************************
! Initialisation Module
! Three-Dimensional Poisson
! **************************************
module init_m
  ! Please do not put any computational variable in this module.
  ! Pass all variables as arguments to the subroutine.
  use precision, only: dp ! use dp as the data-type (and for defining constants!)
  use omp_lib
  implicit none
  private
  public :: first_init_OMP
  public :: init_OMP
  contains

  ! ********************************
  ! **** INITIALISATION ROUTINE **** 
  ! ********************************
  subroutine first_init_OMP(u,f,N)
    real(dp),dimension(:,:,:),intent(inout) :: u,f
    integer :: N,i,j,k
    real(dp) :: delta,d,xi,yi,zi
 
    ! Initial conditions  
    ! Dirichlet boundary conditions
    !print*,'Initialising u matrix...'

    !$OMP PARALLEL DEFAULT(NONE) SHARED(f,u,N,d,delta) PRIVATE(i,j,k,zi,yi,xi)
    u = 0._dp
    !$OMP WORKSHARE
    ! u(:,1,:) = 0.0  ! Not required ?
    u(:,N,:) = 20._dp
    u(1,:,:) = 20._dp
    u(N,:,:) = 20._dp
    u(:,:,1) = 20._dp
    u(:,:,N) = 20._dp
    !$OMP END WORKSHARE

    ! Source function
    !$OMP MASTER
    !print*,'Initialising source function, f matrix...'
    !$OMP END MASTER
    f = 0._dp
    delta = 2._dp/real(N,dp)
    d = real(N,dp)/2._dp

    
    !$OMP DO SCHEDULE(static)
    do k=1,N
      zi = (k-1-d)*delta
      do j=1,N
        yi = (j-1-d)*delta
        do i=1,N
          xi = (i-1-d)*delta
          if ( &
             (xi.ge.(-1._dp)).and.(xi.le.(-3._dp/8._dp)) &
             .and. &
             (yi.ge.(-1._dp)).and.(yi.le.(-1._dp/2._dp)) &
             .and. &
             (zi.ge.(-2._dp/3._dp)).and.(zi.le.0._dp) &
             ) then
               f(i,j,k) = 200._dp
          end if
        end do
      end do
    end do
    !$OMP END DO
    !$OMP END PARALLEL

  end subroutine first_init_OMP

  subroutine init_OMP(u,f,N)
    real(dp),dimension(:,:,:),intent(inout) :: u,f
    integer :: N,i,j,k
    real(dp) :: delta,d,xi,yi,zi
 
    ! Initial conditions  
    ! Dirichlet boundary conditions
    print*,'Initialising u matrix...'
    
    !$OMP PARALLEL DEFAULT(NONE) SHARED(f,u,N,d,delta) PRIVATE(i,j,k,zi,yi,xi)
      !$OMP WORKSHARE
        u = 0._dp
        ! u(:,1,:) = 0.0  ! Not required ?
        u(:,N,:) = 20._dp
        u(1,:,:) = 20._dp
        u(N,:,:) = 20._dp
        u(:,:,1) = 20._dp
        u(:,:,N) = 20._dp
      !$OMP END WORKSHARE

      ! Source function
      !$OMP MASTER
        print*,'Initialising source function, f matrix...'
      !$OMP END MASTER
      
      delta = 2._dp/real(N,dp)
      d = real(N,dp)/2._dp
      
      !$OMP WORKSHARE
        f = 0._dp
      !$OMP END WORKSHARE
      !$OMP DO SCHEDULE(static)
        do k=1,N
          zi = (k-1-d)*delta
          do j=1,N
            yi = (j-1-d)*delta
            do i=1,N
              xi = (i-1-d)*delta
              if ( &
                 (xi.ge.(-1._dp)).and.(xi.le.(-3._dp/8._dp)) &
                 .and. &
                 (yi.ge.(-1._dp)).and.(yi.le.(-1._dp/2._dp)) &
                 .and. &
                 (zi.ge.(-2._dp/3._dp)).and.(zi.le.0._dp) &
                 ) then
                   f(i,j,k) = 200._dp
              end if
            end do
          end do
        end do
      !$OMP END DO
    !$OMP END PARALLEL

  end subroutine init_OMP

end module init_m
