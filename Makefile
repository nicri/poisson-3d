target = poisson3d

.SUFFIXES:
.SUFFIXES: .f90 .o .c

CC = gcc
FC = gfortran
FFLAGS = -O3 -ffast-math -funroll-loops -fopenmp

#CC = suncc
#FC = sunf95
#FFLAGS = -O3 -fopenmp

OBJS = poisson3d.o precision.o init.o jacobi.o gauss_seidel.o write_vtk.o

LIBS =

.PHONY: all
all: $(target)

.PHONY: new
new: clean $(target)

.PHONY: clean realclean
clean:
	@/bin/rm -f $(OBJS) *.mod

realclean: clean
	@/bin/rm -f $(target)

# linking: the target depends on the objects
$(target): $(OBJS)
	$(FC) $(FFLAGS) $(OBJS) -o $(target)

.f90.o:
	$(FC) -c $(FFLAGS) $<

.c.o:
	$(CC) -c $<

# dependencies:
poisson3d.o: precision.o init.o jacobi.o gauss_seidel.o write_vtk.o
init.o: precision.o
jacobi.o: precision.o
gauss_seidel.o: precision.o

