! **************************************
! Gauss-Seidel Module
! Three-Dimensional Poisson
! **************************************
module gauss_seidel_m
  use precision, only: dp ! use dp as the data-type (and for defining constants!)
  use omp_lib
  implicit none
  contains
  
  ! ********************************
  ! ****** SEQUENTIAL ROUTINE ****** 
  ! ********************************
  subroutine gauss_seidel(u,f,N,itermax,tolerance)
    real(dp),dimension(:,:,:),intent(inout) :: u
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k
    real(dp) :: delta, norm, norm_old
    real(dp),intent(in) :: tolerance
    
    print*,'Sequential computing of fields by Gauss-Seidel...' 
    ! Grid spacing
    delta = 2._dp/real(N,dp)
    norm_old = norm2(u)
 
    do istep=1,itermax
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u(i-1,j,k) + u(i+1,j,k)  &
                     + u(i,j-1,k) + u(i,j+1,k)    &
                     + u(i,j,k-1) + u(i,j,k+1)    &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      if (abs(norm2(u)-norm_old).le.tolerance) then
        print*,'Exiting at step: ',istep
        exit
      else
        norm_old = norm2(u)
      end if
    enddo
    print*,'Exiting at step: ',istep-1

  end subroutine gauss_seidel

  subroutine first_gauss_seidel_OMP(u,f,N,itermax)
    real(dp),dimension(:,:,:),intent(inout) :: u
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k,nstep
    real(dp) :: delta
  
    nstep=itermax  
    ! Grid spacing
    delta = 2._dp/real(N,dp)
    print*,'Computing fields by Gauss-Seidel...' 
    !$OMP PARALLEL DEFAULT(NONE) SHARED(nstep,delta,N,u,f) PRIVATE(istep,i,j,k) 
        do istep=1,nstep
        !$OMP DO ORDERED(2)
          do k=2,N-1
            do j=2,N-1
            !$OMP ORDERED DEPEND(sink: k-1,j-1) DEPEND(sink:k-1,j) DEPEND(sink:k-1,j+1) DEPEND(sink:k,j-1)
              do i=2,N-1
                u(i,j,k) = (u(i-1,j,k) + u(i+1,j,k)  &
                + u(i,j-1,k) + u(i,j+1,k)           &
                + u(i,j,k-1) + u(i,j,k+1)           &
                + delta**2*f(i,j,k)) * (1._dp/6._dp)
              enddo
            !$OMP ORDERED DEPEND(source)
            enddo
          enddo
        !$OMP END DO
        enddo
    !$OMP END PARALLEL
  end subroutine first_gauss_seidel_OMP

  subroutine gauss_seidel_OMP(u,f,N,itermax)
  real(dp),dimension(:,:,:),intent(inout) :: u
  real(dp),dimension(:,:,:),intent(in) :: f
  integer :: N,itermax,istep,i,j,k,nstep
  real(dp) :: delta

  nstep=itermax  
  ! Grid spacing
  delta = 2._dp/real(N,dp)
  !print*,'Computing fields by Gauss-Seidel...' 
  !$OMP PARALLEL DEFAULT(NONE) SHARED(nstep,delta,N,u,f) PRIVATE(istep,i,j,k) 
      do istep=1,nstep
      !$OMP DO ORDERED(2) SCHEDULE(STATIC,1)
        do k=2,N-1
          do j=2,N-1
          !$OMP ORDERED  DEPEND(sink:k-1,j) DEPEND(sink:k,j-1)
            do i=2,N-1
              u(i,j,k) = (u(i-1,j,k) + u(i+1,j,k)  &
              + u(i,j-1,k) + u(i,j+1,k)           &
              + u(i,j,k-1) + u(i,j,k+1)           &
              + delta**2*f(i,j,k)) * (1._dp/6._dp)
            enddo
          !$OMP ORDERED DEPEND(source)
          enddo
        enddo
      !$OMP END DO
      enddo
  !$OMP END PARALLEL
  end subroutine gauss_seidel_OMP

  ! ********************************
  ! ***** MEASUREMENT ROUTINE ****** 
  ! ********************************
  subroutine gauss_seidel_measure(u,f,N,itermax,tolerance)
    real(dp),dimension(:,:,:),intent(inout) :: u
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k
    real(dp) :: delta, norm, norm_old
    real(dp),intent(in) :: tolerance
    
    print*,'Sequential computing of fields by Gauss-Seidel...' 
    ! Grid spacing
    delta = 2._dp/real(N,dp)
    norm_old = norm2(u)
 
    do istep=1,itermax
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u(i-1,j,k) + u(i+1,j,k)  &
                     + u(i,j-1,k) + u(i,j+1,k)    &
                     + u(i,j,k-1) + u(i,j,k+1)    &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      if (abs(norm2(u)-norm_old).le.tolerance) then
        print*,'Exiting at step: ',istep
        exit
      else
        open(21,file='gauss_seidel_Unorm.dat',action='write',position='append')
        write(21,*) norm2(u)
        close(21)
        norm_old = norm2(u)
      end if
    enddo

  end subroutine gauss_seidel_measure
end module gauss_seidel_m
  
