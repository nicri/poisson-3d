! **************************************
! Main Program
! Three-Dimensional Poisson
! **************************************
program poisson3d
  ! Import different methods
  use precision
  use init_m
  use jacobi_m
  use gauss_seidel_m
  ! Define options for user
  ! Please note that while we assign on definition, save is implicit.
  ! But please write save to show intent.
  integer, save :: N = 100
  real(dp), save :: T0 = 0._dp
  integer, save :: itermax = 1000
  real(dp), save :: tolerance = 1.e-4_dp
  integer, save :: output = 0    ! 3 for binary, 4 for VTK
  integer, save :: algorithm = 1 ! 1 for Jacobi and 2 for Gauss-Seidel
  real(dp), allocatable, dimension(:,:,:) :: u, u_old, f
  integer :: iostat
  character(8)  :: date
  character(10) :: time
  character(5)  :: zone
  integer,dimension(8) :: values
  integer :: tw1,tw2,clock_rate

  ! Read in namelist.
  ! Call program namelist
  ! Create a file called: input.dat
  ! The content may be:
  ! &INPUT
  !   N = 100
  !   itermax = 3000
  !   T0 = 10.
  !   tolerance = 1e-5
  !   output = 0
  !   algorithm = 1
  ! /
  ! If one of the keywords are not in the
  ! input.dat namelist, then the default value
  ! will be used.
  call read_namelist()

  ! Array allocation
  ! Allocate arrays
  print*,'Allocating u matrix...' 
  allocate(u(N,N,N), stat=iostat)
  call check_iostat(iostat, "Could not allocate 'u' matrix!")
  print*,'Allocating source function, f matrix...'
  allocate(f(N,N,N), stat=iostat)
  call check_iostat(iostat, "Could not allocate 'f' matrix!")

  ! ********************************************************
  print*,'START'
  call date_and_time(date,time,zone,values)
  print*,'DATE: ',values(3),'/',values(2),'/',values(1)
  print*,'TIME: ',values(5),':',values(6),':',values(7),'.',values(8)
  print*

  ! Initial conditions
  call init_OMP(u,f,N) 

  select case (algorithm)
  case (1)
    print*,'Using Jacobi algorithm.' 
    print*,'Allocating duplicate u matrix for Jacobi...' 
    allocate(u_old(N,N,N), stat=iostat)
    call check_iostat(iostat, "Could not allocate duplicate 'u' matrix!")
    ! Call time-step update method
    call system_clock(tw1,clock_rate)
    call jacobi_measure(u,u_old,f,N,itermax,tolerance)
    call system_clock(tw2,clock_rate)
    deallocate(u_old)
  case (2)
    print*,'Using Gauss-Seidel algorithm.'
    ! Call time-step update method
    call system_clock(tw1,clock_rate)
    call gauss_seidel_measure(u,f,N,itermax,tolerance)
    call system_clock(tw2,clock_rate)
  case default
    write(*,'(a,i0)') 'Unknown algorithm type requested: ', algorithm
    stop
  end select

  call date_and_time(date,time,zone,values)
  print*
  print*,'DATE: ',values(3),'/',values(2),'/',values(1)
  print*,'TIME: ',values(5),':',values(6),':',values(7),'.',values(8)
  print*
  print*,'Wall clock time of computations:',(tw2-tw1)/real(clock_rate),'s'
  print*,'END'
  ! ********************************************************

  deallocate(f)

  ! Keep u until we have written in out
  select case ( output )
  case ( 0 ) ! pass, valid but not used value
    ! do nothing
  case ( 3 ) ! write to binary file
    call write_binary(u)
  case ( 4 ) ! write to binary file
    call write_vtk(u)
  case default

    write(*,'(a,i0)') 'Unknown output type requested: ', output
    stop

  end select
    
  deallocate(u)

  ! **************************************
  ! Input/Output
  ! **************************************
  contains
  ! ********************************
  ! **** INPUT NAMELIST ROUTINE **** 
  ! ********************************
  subroutine read_namelist()
    integer :: unit,io_err
    
    namelist /INPUT/ N, itermax, T0, tolerance, output, algorithm
    
    ! open and read file
    unit = 128273598
    open(unit, FILE="input.dat", action='read', iostat=io_err)
    call check_iostat(io_err, &
        "Could not open file 'input.dat', perhaps it does not exist?")
    read(unit, nml=INPUT, iostat=io_err)
    call check_iostat(io_err, &
        "Error on reading name-list, please correct file content")
    close(unit)

  end subroutine read_namelist

  ! ********************************
  ! ***** CHECK IN/OUT ROUTINE ***** 
  ! ********************************
  subroutine check_iostat(iostat, msg)
    integer, intent(in) :: iostat
    character(len=*), intent(in) :: msg

    if ( iostat == 0 ) return

    write(*,'(a,i0,/,tr3,a)') 'ERROR = ', iostat, msg
    stop

  end subroutine check_iostat

  ! ********************************
  ! ***** WRITE BINARY ROUTINE ***** 
  ! ********************************
  subroutine write_binary(u)
    ! Array to write-out
    real(dp), intent(in) :: u(:,:,:)

    ! Local variables
    character(len=*), parameter :: filename = 'poisson_u.bin'
    integer :: N
    integer :: iostat

    integer, parameter :: unit = 11249

    ! Get size of the array
    N = size(u, 1)

    ! replace == overwrite
    open(unit,file=filename,form='unformatted',access='stream',status='replace', iostat=iostat)
    call check_iostat(iostat, "Could not open file '"//filename//"'!")
    write(unit, iostat=iostat) N
    call check_iostat(iostat, "Could not write variable N")
    write(unit, iostat=iostat) u
    call check_iostat(iostat, "Could not write variable u")
    close(unit)

  end subroutine write_binary

  ! ********************************
  ! ******* WRITE VTK ROUTINE ****** 
  ! ********************************
  subroutine write_vtk(u)

    ! This is C-interoperability using bindings
    use, intrinsic :: iso_c_binding

    interface
      subroutine write_vtk_c(n, u) BIND(C, name="write_vtk")
        import c_ptr, c_int
        implicit none
        integer(c_int), value :: n
        type(c_ptr), value :: u
      end subroutine
    end interface

    ! Array to write-out
    real(dp), intent(in), target :: u(:,:,:)

    ! Local variables
    integer(c_int) :: N

    ! Get size of the array
    N = size(u, 1)

    call write_vtk_c(n, c_loc(u(1,1,1)))

  end subroutine write_vtk
  
end program poisson3d


