! **************************************
! Jacobi Module
! Three-Dimensional Poisson
! **************************************
module jacobi_m
  use precision, only: dp ! use dp as the data-type (and for defining constants!)
  use omp_lib
  implicit none
  contains
  
  ! ********************************
  ! ****** SEQUENTIAL ROUTINE ****** 
  ! ********************************
  subroutine jacobi(u,u_old,f,N,itermax,tolerance)
    real(dp),dimension(:,:,:),intent(inout) :: u,u_old
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k
    real(dp) :: delta
    real(dp),intent(in) :: tolerance
    
    print*,'Sequential computing of fields by Jacobi...' 
    ! Grid spacing
    delta = 2._dp/real(N,dp)

    ! Duplicate u matrix values 
    u_old = u  
    ! Perform time iterations
    do istep=1,itermax
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u_old(i-1,j,k) + u_old(i+1,j,k) &
                     + u_old(i,j-1,k) + u_old(i,j+1,k)   &
                     + u_old(i,j,k-1) + u_old(i,j,k+1)   &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      if (abs(norm2(u)-norm2(u_old)).le.tolerance) then
        print*,'Exiting at step: ',istep
        exit
      else
        ! Update old (duplicate) field
        u_old = u
      end if  
    enddo
    print*,'Exiting at step: ',istep-1

  end subroutine jacobi
  
  subroutine first_jacobi_OMP(u,u_old,f,N,itermax)
    real(dp),dimension(:,:,:),intent(inout) :: u,u_old
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k,nstep
    real(dp) :: delta

   
    !print*,'Computing fields by Jacobi...' 
    nstep=itermax
    ! Grid spacing
    delta = 2._dp/real(N,dp)

    ! Duplicate u matrix values 
    !$OMP PARALLEL DEFAULT(NONE) SHARED(nstep,delta,N,u,u_old,f) PRIVATE(istep,i,j,k) 
    u_old = u  
    ! Perform time iterations

    do istep=1,nstep
    !$OMP DO COLLAPSE(3) SCHEDULE(runtime)
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u_old(i-1,j,k) + u_old(i+1,j,k) &
                     + u_old(i,j-1,k) + u_old(i,j+1,k)   &
                     + u_old(i,j,k-1) + u_old(i,j,k+1)   &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      ! Update old (duplicate) field
      !$OMP END DO
      u_old = u  
    enddo
    !$OMP END PARALLEL 

  end subroutine first_jacobi_OMP

  subroutine jacobi_OMP(u,u_old,f,N,itermax)
    real(dp),dimension(:,:,:),intent(inout) :: u,u_old
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k,nstep
    real(dp) :: delta

   
    !print*,'Computing fields by Jacobi...' 
    nstep=itermax
    ! Grid spacing
    delta = 2._dp/real(N,dp)

    ! Duplicate u matrix values 
    !$OMP PARALLEL DEFAULT(NONE) SHARED(nstep,delta,N,u,u_old,f) PRIVATE(istep,i,j,k) 
    !$OMP WORKSHARE
    u_old = u  
    !$OMP END WORKSHARE
    ! Perform time iterations

    do istep=1,nstep
    !$OMP DO SCHEDULE(static)
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u_old(i-1,j,k) + u_old(i+1,j,k) &
                     + u_old(i,j-1,k) + u_old(i,j+1,k)   &
                     + u_old(i,j,k-1) + u_old(i,j,k+1)   &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      ! Update old (duplicate) field
      !$OMP END DO
      !$OMP WORKSHARE
      u_old = u  
      !$OMP END WORKSHARE
    enddo
    !$OMP END PARALLEL 

  end subroutine jacobi_OMP



  ! ********************************
  ! ***** MEASUREMENT ROUTINE ****** 
  ! ********************************
  subroutine jacobi_measure(u,u_old,f,N,itermax,tolerance)
    real(dp),dimension(:,:,:),intent(inout) :: u,u_old
    real(dp),dimension(:,:,:),intent(in) :: f
    integer :: N,itermax,istep,i,j,k
    real(dp) :: delta
    real(dp),intent(in) :: tolerance
    
    print*,'Sequential computing of fields by Jacobi...' 
    ! Grid spacing
    delta = 2._dp/real(N,dp)

    ! Duplicate u matrix values 
    u_old = u  
    ! Perform time iterations
    do istep=1,itermax
      do k=2,N-1
        do j=2,N-1
          do i=2,N-1
            u(i,j,k) = ( u_old(i-1,j,k) + u_old(i+1,j,k) &
                     + u_old(i,j-1,k) + u_old(i,j+1,k)   &
                     + u_old(i,j,k-1) + u_old(i,j,k+1)   &
                     + delta**2*f(i,j,k) ) * (1._dp/6._dp)
          enddo
        enddo
      enddo
      if (abs(norm2(u)-norm2(u_old)).le.tolerance) then
        print*,'Exiting at step: ',istep
        exit
      else
        open(21,file='jacobi_Unorm.dat',action='write',position='append')
        write(21,*) norm2(u)
        close(21)
        ! Update old (duplicate) field
        u_old = u
      end if  
    enddo

  end subroutine jacobi_measure
end module jacobi_m
